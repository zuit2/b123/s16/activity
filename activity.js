// function to display a multiplication table of 5
function displayMultiplicationTable() {

	for(n=1;n<=10;n++){
		console.log(`5 x ${n}`);
	}
}

displayMultiplicationTable();


// function should delete all letters "a" and "e" from string
function removeAandE(str){

	strHolder=""
	for(i=0;i<str.length;i++){
		if(str[i]!=="a" && str[i]!=="e" && str[i]!=="A" && str[i]!=="E"){
			strHolder += str[i];
		}
		// console.log(strHolder);
	}
	return strHolder;
}

let oldString = "The best president in the galaxy!"
let newString = removeAandE(oldString);
console.log(newString);